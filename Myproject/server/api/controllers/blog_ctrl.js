var mongoose = require('mongoose');
var blogform = mongoose.model('blog');
var commentform = mongoose.model('comment');
var fs = require('fs');
module.exports = {
    addBlogData: addBlogData,
    listBlog: listBlog,
    deleteBlog: deleteBlog,
    editBlog: editBlog,
    getBlogDetail: getBlogDetail,
    addComment: addComment,
    listComment: listComment,
    deleteComment: deleteComment,
}

function addBlogData(req, res) {
    //Uplaod Image
    timeStamp = Date.now();
    orignalImageName = timeStamp + "_" + req.files.file[0].originalname;
    var imagePath = '../server/public/bloguploads/' + orignalImageName;
    fs.writeFile(imagePath, (req.files.file[0].buffer), function (err) {
        if (err) throw err;
        console.log("Product Image Uploaded");
    })

    var blog = new blogform();
    blog.title = req.body.title;
    blog.description = req.body.description;
    blog.imagePath = "http://localhost:10010/bloguploads/" + orignalImageName;
    blog.isdelete = false;
    blog.date = Date.now();


    blog.save(function (err, data) {
        if (err) {
            res.json({ "code": 400, "message": "error" });
        }
        else {

            res.json({ "code": 200, "message": "success" });
        }
    });
}

function listBlog(req, res) {
    blogform.find({isdelete:false},function (err, data) {
        res.json(data);
    })
}

function deleteBlog(req, res) {
    var id = req.swagger.params.id.value;
    console.log("..id", id)
    blogform.findByIdAndUpdate({ _id: id },{ $set: { isdelete:true }} , function (err, data) {
        if (err) {
            throw err;
        }
        else {
            res.json(data);
        }
    });
}

function editBlog(req, res) {
    let _id = req.swagger.params.id.value
    console.log(_id);
    timeStamp = Date.now();
    orignalImageName = timeStamp + "_" + req.files.file[0].originalname;
    var imagePath = '../server/public/bloguploads/' + orignalImageName;
    fs.writeFile(imagePath, (req.files.file[0].buffer), function (err) {
        if (err) throw err;
        console.log("Product Image Uploaded");
    })

    var title = req.body.title;
    var description = req.body.description;
    var date = Date.now();
    var imagePath = "http://localhost:10010/bloguploads/" + orignalImageName;

    blogform.findByIdAndUpdate(_id, { $set: { title: title, description: description, imagePath: imagePath, date: date} }, function (err, data) {
        if (err) {
            console.log("errr", err)
        } else {
            console.log("data", data)
        }
        data.save();
        res.json({ data: data, code: 200 });
    });
};

function getBlogDetail(req, res) {
    var _id = req.swagger.params.id.value
    blogform.findById(_id, function (err, userData) {
        if (err) {
            res.json({
                message: "error"
            })
        } else if (userData) {
            res.json({
                message: "success",
                data: userData
            })
        }
        else {
            res.json({
                message: "error",
            })
        }
    })
}

function addComment(req, res) {
    console.log(req.body);
    let addcomment = new commentform({
        userid: req.body.userid,
        blogid: req.body.blogid,
        comment: req.body.comment,
        date: Date.now(),
        rating: req.body.rating,
    });
    // var content = req.body;
    // var Form = new form(req.body);
    addcomment.save(function (err, data) {
        if (err) {
            res.json({ "code": 400, "message": "error" });
        }
        else {

            res.json({ "code": 200, "message": "success" });
        }
    });
}

function listComment(req, res) {
  var  blogid = req.swagger.params.id.value;
    commentform.find({blogid,isdelete:false}).sort({date:'descending'}).populate({
   path:'userid',
   model:'signup'
    }).populate({
        path:'blogid',
        model:'blog'
    }).exec(function (err, data) {
        res.json(data);
    })
}   

function deleteComment(req, res) {
    var id = req.swagger.params.id.value;
    //console.log("..id", id)
    commentform.findByIdAndUpdate({ _id: id },{ $set: { isdelete:true }}, function (err, data) {
        if (err) {
            throw err;
        }
        else {
            res.json(data);
        }
    });
}






