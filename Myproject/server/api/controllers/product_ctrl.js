var mongoose = require('mongoose');
var productform = mongoose.model('product');
var fs = require('fs');
module.exports = {
    addProductData: addProductData,
    listProduct: listProduct,
    deleteProduct: deleteProduct,
    editProduct: editProduct,
    getProductDetail: getProductDetail,
}

function addProductData(req, res) {
    //Uplaod Image
    timeStamp = Date.now();
    orignalImageName = timeStamp + "_" + req.files.file[0].originalname;
    var imagePath = '../server/public/uploads/' + orignalImageName;
    fs.writeFile(imagePath, (req.files.file[0].buffer), function (err) {
        if (err) throw err;
        console.log("Product Image Uploaded");
    })

    var product = new productform();
    product.productname = req.body.productname;
    product.productcompanyname = req.body.productcompanyname;
    product.productdescription = req.body.productdescription;
    product.productcategory = req.body.productcategory;
    product.productcost = req.body.productcost;
    product.imagePath = "http://localhost:10010/uploads/" + orignalImageName;
    product.isdelete = false;

    product.save(function (err, data) {
        if (err) {
            res.json({ "code": 400, "message": "error" });
        }
        else {

            res.json({ "code": 200, "message": "success" });
        }
    });
}

function listProduct(req, res) {
    productform.find({isdelete:false},function (err, data) {
        res.json(data);
    })
}


function deleteProduct(req, res) {
    var id = req.swagger.params.id.value;
    console.log("..id", id)
    productform.findByIdAndUpdate({ _id: id },{ $set: { isdelete:true }} ,function (err, data) {
        if (err) {
            throw err;
        }
        else {
            res.json(data);
        }
    });
}

function getProductDetail(req, res) {
    var _id = req.swagger.params.id.value
    productform.findById(_id, function (err, userData) {
        if (err) {
            res.json({
                message: "error"
            })
        } else if (userData) {
            res.json({
                message: "success",
                data: userData
            })
        }
        else {
            res.json({
                message: "error",
            })
        }
    })
}


function editProduct(req, res) {
    let _id = req.swagger.params.id.value
    console.log(_id);
    timeStamp = Date.now();
    orignalImageName = timeStamp + "_" + req.files.file[0].originalname;
    var imagePath = '../server/public/uploads/' + orignalImageName;
    fs.writeFile(imagePath, (req.files.file[0].buffer), function (err) {
        if (err) throw err;
        console.log("Product Image Uploaded");
    })

    var productname = req.body.productname;
    var productcompanyname = req.body.productcompanyname;
    var productdescription = req.body.productdescription;
    var productcategory = req.body.productcategory;
    var productcost = req.body.productcost;
    var imagePath = "http://localhost:10010/uploads/" + orignalImageName;

    productform.findByIdAndUpdate(_id, { $set: { productname: productname, productcompanyname: productcompanyname, productdescription: productdescription, productcategory: productcategory, productcost: productcost, imagePath: imagePath } }, function (err, data) {
        if (err) {
            console.log("errr", err)
        } else {
            console.log("data", data)
        }
        data.save();
        res.json({ data: data, code: 200 });
    });
};
