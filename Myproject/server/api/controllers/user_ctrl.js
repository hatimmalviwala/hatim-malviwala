var mongoose = require('mongoose');
var form = mongoose.model('user');
var jwt = require('jsonwebtoken');
var fs = require('fs');
const bcrypt = require('bcrypt');

module.exports = {
    signupdata: signupdata,
    logindata: logindata,
    listuser: listuser,
    addUser: addUser,
    deleteUser: deleteUser,
    editUser: editUser,
    getUserDetail: getUserDetail,
}
function signupdata(req, res) {
   // console.log("body====================",req.body);
     const saltRounds = 10;
     const myPlaintextPassword = req.body.password;
     //console.log("password",myPlaintextPassword)
     //var encryptPassword;
    // var hash = bcrypt.hashSync(myPlaintextPassword, saltRounds);
    bcrypt.hash(myPlaintextPassword, saltRounds, function(err, hash) {
    if (err){
        console.log("error",err);
    }
    else{
  // Store hash in your password DB.
        console.log("hash",hash)
        //encryptPassword = hash;
        let signupdata = new form({
        fullname: req.body.fullname,
        email: req.body.email,
        password: hash,
        isdelete:false,
        role: "user",
    });
        signupdata.save(function (err, data) {
        if (err) {
            console.log("error",err);
            res.json({"code":500,"password":err})
        }
        else {
                 console.log("success",data);
            res.json({"code":200,"data":data});
        }
    });
    }
});

}

function logindata(req, res) {
    const saltRounds = 10;
    var myPlaintextPassword = req.body.password;
    
    //  var originalPassword;
    console.log("hit")
  
    let data = {
        email: req.body.email,
        //password: req.body.password,
        isdelete:false
    };
    form.findOne(data).lean().exec(function (err, user) {
        console.log(user);
        var hashPassword = user.password;
        //console.log("password",hashPassword);
        if (err) {
            console.log("kbkbmkbmgklbmkgfhmg", err)
            return res.json({ error: true });

        }
           
            //console.log(id);
        if (!user) {
            console.log("not user", user)
            return res.json({ 'message': 'not user' });

        }
        // console.log("global.config.jwt_secret",global.config.jwt_secret);
        
          bcrypt.compare(myPlaintextPassword, hashPassword, function(err, response) {
        if(response == true){
                    var id = user._id;
        let token = jwt.sign( {"id" : id} , 'hatim', {

            expiresIn: "2h" // expires in 2 hours

        });
        console.log(token);
        res.json({ msg: 'Token is created', token: token, data: user });

        }
        else{
        res.json({ 'message': 'not user'});    
        }
        });
 
    })

}

function listuser(req, res) {
    form.find({isdelete:false},function (err, data) {
        res.json(data);
    })
}

function addUser(req, res) {
     const saltRounds = 10;
     const myPlaintextPassword = req.body.password;
     bcrypt.hash(myPlaintextPassword, saltRounds, function(err, hash) {
    if (err){
        console.log("error",err);
    }
    else{    
    console.log(req.body);
    let adduserdata = new form({
        fullname: req.body.fullname,
        email: req.body.email,
        password: hash,
        role: req.body.role,
    });
    // var content = req.body;
    // var Form = new form(req.body);
    adduserdata.save(function (err, data) {
        if (err) {
            res.json({ "code": 400, "message": "error" });
        }
        else {

            res.json({ "code": 200, "message": "success" });
        }
    });
    }
});
}

function deleteUser(req, res) {
    var id = req.swagger.params.id.value;
    console.log("..id", id)
    form.findByIdAndUpdate({ _id: id },{ $set: { isdelete:true }}, function (err, data) {
        if (err) {
            throw err;
        }
        else {
            res.json(data);
        }
    });
}

function editUser(req, res) {
    let _id = req.swagger.params.id.value
    console.log(_id);
    var fullname = req.body.fullname;
    var email = req.body.email;
    var password = req.body.password;
    var role = req.body.role;

    form.findByIdAndUpdate(_id, { $set: { fullname: fullname, email: email, password: password, role: role } }, function (err, data) {
        if (err) {
            console.log("err", err)
        } else {
            console.log("data", data)
        }
        data.save();
        res.json({ data: data, code: 200 });
    });
};


function getUserDetail(req, res) {
    var _id = req.swagger.params.id.value
    form.findById(_id, function (err, userData) {
        if (err) {
            res.json({
                message: "error"
            })
        } else if (userData) {
            res.json({
                message: "success",
                data: userData
            })
        }
        else {
            res.json({
                message: "error",
            })
        }
    })
}
