var mongoose = require('mongoose');
var cartform = mongoose.model('cart');
module.exports = {
        addcart: addcart,
        listcart: listcart,
        deletecart: deletecart,
        // updateCart: updateCart        
}


function addcart(req, res) {
        let addcart = new cartform({
        userid: req.body.userid,
        productid: req.body.productid,
        productprice: req.body.productprice,
        quantity: 1,
        date: Date.now(),
    });

    cartform.findOne({ productid: req.body.productid ,isdelete:false}, function (err, result) {
        if (err) {
            consol.log(err);
        } else if (result) {
            console.log(result);
            quant = result.quantity+1
            totalPrice =   quant * addcart.productprice;
            cartform.findOneAndUpdate({productid :req.body.productid,isdelete:false},{$set:{productprice:totalPrice, quantity:quant}}, function(err, result){
                if(err){
                    console.log(err);
                }else{
                    res.json({
                        code:200,
                        message:"success",
                        data:result
                    })
                }
            })
        }
        else {
            addcart.save(function (err, response) {
                if (err) {
                    res.json({
                        code: 404,
                        message: 'comment not Added',
                        // data:response
                    })
                }  else {
                            res.json({
                                code: 200,
                                data: response
                            })
                        }
            })
        }
    })
}


function listcart(req, res) {
  var  userid = req.swagger.params.id.value;
    cartform.find({userid,isdelete:false}).sort({date:'descending'}).populate({
   path:'productid',
   model:'product'
    }).exec(function (err, data) {
        res.json(data);
    })
}   


function deletecart(req, res) {
    var cartid = req.swagger.params.id.value;
    cartform.findByIdAndRemove( cartid ,{ $set: { isdelete:true }} ,function (err, data) {
        if (err) {
            throw err;
        }
        else {
            res.json(data);
        }
    });
}

//  function updateCart(req, res) {
//     let userid = req.body.id
//     let productid = req.body.id
//     var quantity = req.body.quantity;
//     var productprice = quantity * req.body.productprice;

//     cartform.findByIdAndUpdate({ userid, productid, isdelete:false }, { $set: { quantity: quantity, productprice: productprice } }, function (err, data) {
//         if (err) {
//             console.log("err", err)
//         } else {
//             console.log("data", data)
//         }
//         data.save();
//         res.json({ data: data, code: 200 });
//     });
// };
