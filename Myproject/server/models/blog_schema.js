"use strict"
var mongoose = require('mongoose');

//Define a schema
var Schema = mongoose.Schema;

var blogdata = new mongoose.Schema({
    title: { type: String },
    description: { type: String },
    imagePath: { type: String },
    date:{type: Date},
    isdelete: { type : Boolean }


});

var blog = mongoose.model('blog', blogdata);
module.export = blog;