"use strict"
var mongoose = require('mongoose');

//Define a schema
var Schema = mongoose.Schema;

var cartdata = new mongoose.Schema({
    userid: { type: String ,ref:'signup' },
    productid: { type: String ,ref:'product' },
    productprice: {type: Number },
    quantity: { type: Number },
    date:{ type:Date },
    isdelete: { type : Boolean, default:false }


});

var cart = mongoose.model('cart', cartdata);
module.export = cart;