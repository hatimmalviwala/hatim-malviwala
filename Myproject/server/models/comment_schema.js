"use strict"
var mongoose = require('mongoose');

//Define a schema
var Schema = mongoose.Schema;

var commentdata = new mongoose.Schema({
    userid: { type: String ,ref:'signup'},
    blogid: { type: String ,ref:'blog' },
    rating: {type: Number},
    comment: { type: String },
    date:{type:Date},
    isdelete: { type : Boolean, default:false }


});

var comment = mongoose.model('comment', commentdata);
module.export = comment;