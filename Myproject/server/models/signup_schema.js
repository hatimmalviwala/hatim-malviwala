"use strict"
var mongoose = require('mongoose');

//Define a schema
var Schema = mongoose.Schema;

var formdata = new mongoose.Schema({
    fullname: { type: String , required:true },
    email: { type: String ,required:true },
    password: { type: String,required:true},
    isdelete: { type: Boolean, default: false },
    role: { type: String },
});

var user = mongoose.model('user', formdata);
module.export = user;