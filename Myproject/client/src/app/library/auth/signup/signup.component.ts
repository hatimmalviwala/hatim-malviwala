import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from "@angular/router";
import { AuthService } from "src/app/library/service/auth.service";
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
myform: FormGroup;
submitted = false;

  constructor(private auth: AuthService,private router: Router,private toastr:ToastrService) { }

  ngOnInit() {
    this.myform = new FormGroup({
      'fullname': new FormControl('', [Validators.required]),
      'password': new FormControl('', [Validators.required, Validators.minLength(6)]),
      'email': new FormControl('', [Validators.required, Validators.email]),
    });
  }
   get f() { return this.myform.controls; }
   
   onsubmit() {
     this.submitted = true;
 
        // stop here if form is invalid
        if (this.myform.invalid) {
            return;
        }    
       this.auth.signup(this.myform.value).subscribe((res: any) => {
       res = res.data;
       this.toastr.success("signup successfully");
       console.log('response ::',res);
     });
     this.router.navigate(['auth/login']);
  }
}
