import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { Validators } from '@angular/forms';
import { AuthService } from "src/app/library/service/auth.service";
import { Router } from "@angular/router";
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  submitted = false;

  constructor(private auth: AuthService, private router: Router,private toastr:ToastrService) { }

  ngOnInit() {
    this.loginForm = new FormGroup({
      'email': new FormControl('', [Validators.required, Validators.email]),
      'password': new FormControl('', [Validators.required, Validators.minLength(6)]),
    });
  }
  get f() { return this.loginForm.controls; }
onsubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    }
    console.log(this.loginForm.value);
    this.auth.login(this.loginForm.value).subscribe((res: any) => {
      //   res = res.data;
      console.log('response ::', res);
      if (res.message == 'not user'){
        this.toastr.error("Incorrect e-mail or password!!");
      }
      //alert("Signup successful");
      //this.toastr.success("login successfully");
      else{
      var data = res.data
      localStorage.setItem("fullname", data.fullname);
      localStorage.setItem("userid",data._id)
      localStorage.setItem("role", data.role);
      var token = res.token;
      localStorage.setItem("token", token);  
      if((localStorage.getItem("token") != null) && localStorage.getItem("role") == "admin")
      {
        this.router.navigate(['/admin/dashboard'])
      }
      else{
        this.router.navigate(['/user/dashboard'])
      }
      this.toastr.success("login successfully");
      }
    })
}
}
