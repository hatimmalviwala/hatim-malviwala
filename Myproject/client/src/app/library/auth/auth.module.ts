import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SignupComponent } from './signup/signup.component';
import { LoginComponent } from './login/login.component';
import { AuthRouting } from "src/app/library/auth/auth.routing";
import { ReactiveFormsModule } from '@angular/forms';
import {PasswordModule} from 'primeng/password';

@NgModule({
  imports: [
    CommonModule,
    AuthRouting,
    ReactiveFormsModule,
    PasswordModule
  ],
  declarations: [SignupComponent, LoginComponent]
})
export class AuthModule { }
