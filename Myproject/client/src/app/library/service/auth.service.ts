import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { AddUserComponent } from 'src/app/areas/admin/manageUsers/add-user/add-user.component';
@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient, myRoute: Router) { }

  signup(data: any): Observable<any> {
      console.log('data', data);
      return this.http.post('http://localhost:10010/api/signupdata/', data);
  }
  login(data: any): Observable<any> {
      console.log('data', data);
      return this.http.post('http://localhost:10010/api/logindata/', data);
  }
  listuser():Observable<any>{
    return this.http.get('http://localhost:10010/api/listuser');
  }
  addUser(data: any): Observable<any> {
      console.log('data', data);
      return this.http.post('http://localhost:10010/api/addUser/', data);
  }
  deleteUser(id:any):Observable<any>{
  console.log(id);
    return this.http.delete('http://localhost:10010/api/deleteUser?id=' + id );
  }
  updateUser(id:any,data:any):Observable<any>{
  console.log(id,data);
  return this.http.put('http://localhost:10010/api/editUser?id=' + id,data);
  }

  getUserDetails(id:any):Observable<any>{
  console.log(id);
  return this.http.get('http://localhost:10010/api/getUserDetail?id=' + id);
  }



  getToken() {
    return localStorage.getItem("token");
  }
  isLoggednIn() {
    return this.getToken() !== null;
  }

}
