import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class BlogService {

  constructor(private http: HttpClient, myRoute: Router) { }

    addBlog(data: any): Observable<any> {
      console.log('data', data);
      return this.http.post('http://localhost:10010/api/addBlog', data);
  }
    listBlog():Observable<any>{
    return this.http.get('http://localhost:10010/api/listBlog');
  }
    deleteBlog(id:any):Observable<any>{
  console.log(id);
    return this.http.delete('http://localhost:10010/api/deleteBlog?id=' + id);
  }  
    editBlog(id:any,data:any):Observable<any>{
  console.log(id,data);
  return this.http.put('http://localhost:10010/api/editBlog?id=' + id,data);
  }

  getBlogDetails(id:any):Observable<any>{
  console.log(id);
  return this.http.get('http://localhost:10010/api/getBlogDetail?id=' + id);
  }


}
