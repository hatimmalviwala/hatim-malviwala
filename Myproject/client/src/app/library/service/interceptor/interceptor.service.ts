import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpHandler, HttpRequest, HttpEvent, HttpResponse }from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class InterceptorService implements HttpInterceptor {

  constructor() { }
    intercept(req: HttpRequest<any>, next: HttpHandler) {
    var token = localStorage.getItem("token");
    const authRequest = req.clone({
        headers:req.headers.set("authorization",'Bearer ' + token)
    });

    return next.handle(authRequest);
  }
}
