import { TestBed, inject } from '@angular/core/testing';

import { BlogDetailService } from './blog-detail.service';

describe('BlogDetailService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BlogDetailService]
    });
  });

  it('should be created', inject([BlogDetailService], (service: BlogDetailService) => {
    expect(service).toBeTruthy();
  }));
});
