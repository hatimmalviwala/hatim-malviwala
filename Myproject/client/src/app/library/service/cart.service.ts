import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
@Injectable({
  providedIn: 'root'
})
export class CartService {

  constructor(private http: HttpClient, myRoute: Router) { }
      addCart(data: any): Observable<any> {
      console.log('data', data);
      return this.http.post('http://localhost:10010/api/addCart', data);
  }

      getCartDetails(id:any):Observable<any>{
      console.log(id);
      return this.http.get('http://localhost:10010/api/listCart?id=' + id);
  }

      deleteCart(id:any):Observable<any>{
      console.log(id);
      return this.http.delete('http://localhost:10010/api/deleteCart?id=' + id);
  }

}
