import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class BlogDetailService {

  constructor(private http: HttpClient, myRoute: Router) { }

  getBlogDetails(id:any):Observable<any>{
  console.log(id);
  return this.http.get('http://localhost:10010/api/listComment?id=' + id);
  }

  addComment(data: any): Observable<any> {
  console.log('data', data);
  return this.http.post('http://localhost:10010/api/addComment', data);
  }

  deleteComment(id:any):Observable<any>{
  console.log(id);
    return this.http.delete('http://localhost:10010/api/deleteComment?id=' + id );
  }
}
