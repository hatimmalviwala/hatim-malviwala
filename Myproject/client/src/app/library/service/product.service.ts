import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private http: HttpClient, myRoute: Router) { }

  addProduct(data: any): Observable<any> {
      console.log('data', data);
      return this.http.post('http://localhost:10010/api/addProduct', data);
  }
  listProduct():Observable<any>{
    return this.http.get('http://localhost:10010/api/listProduct');
  }
  deleteProduct(id:any):Observable<any>{
  console.log(id);
    return this.http.delete('http://localhost:10010/api/deleteProduct?id=' + id);
  }
  editProduct(id:any,data:any):Observable<any>{
  console.log(id,data);
  return this.http.put('http://localhost:10010/api/editProduct?id=' + id,data);
  }

  getProductDetails(id:any):Observable<any>{
  console.log(id);
  return this.http.get('http://localhost:10010/api/getProductDetail?id=' + id);
  }

}
