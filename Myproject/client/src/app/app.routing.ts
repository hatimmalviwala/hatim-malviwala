import { NgModule } from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import { HomeComponent } from 'src/app/library/shared/home/home.component';
import { AuthGuard } from "src/app/guard/auth.guard";
import { UserGuard } from "src/app/guard/user.guard";
import { LoginGuard } from "src/app/guard/login.guard";

const routes: Routes = [
    {
        path: '',
        component: HomeComponent,
        canActivate: [LoginGuard]
    },
    {
        path: 'auth',
        loadChildren: './library/auth/auth.module#AuthModule',
        canActivate: [LoginGuard]
    },
    {
        path: 'admin',
        loadChildren: './areas/admin/admin.module#AdminModule',
        canActivate: [AuthGuard]
    },
    {
        path: 'user',
        loadChildren: './areas/user/user.module#UserModule',
        canActivate: [UserGuard]
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
})
export class AppRouting {}
