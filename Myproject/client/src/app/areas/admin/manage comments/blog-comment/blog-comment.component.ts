import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BlogDetailService } from "src/app/library/service/blog-detail.service";
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-blog-comment',
  templateUrl: './blog-comment.component.html',
  styleUrls: ['./blog-comment.component.css']
})
export class BlogCommentComponent implements OnInit {
  commentList: any;

  constructor(private route:ActivatedRoute, private auth:BlogDetailService,private toastr:ToastrService) { }

  ngOnInit() {
        this.route.params.subscribe(params => {
      this.auth.getBlogDetails(params.id).subscribe(res => {
        this.commentList = res;
        //console.log("data",this.commentList)
      })
    })
  }
    deleteComment(id) {
    console.log("...id");
     this.auth.deleteComment(id).subscribe(res => {
     //console.log('Deleted',res);
    //   if(res.status == "success"){
    //     console.log("msg",res.msg);
         this.ngOnInit();
    //   }else{
    //     console.log("fdgdgdfgfd")
    //   }
     });
  this.toastr.success("Comment deleted successfully");
}

}
