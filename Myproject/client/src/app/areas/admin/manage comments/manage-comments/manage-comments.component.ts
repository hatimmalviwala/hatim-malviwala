import { Component, OnInit } from '@angular/core';
import { BlogService } from "src/app/library/service/blog.service";

@Component({
  selector: 'app-manage-comments',
  templateUrl: './manage-comments.component.html',
  styleUrls: ['./manage-comments.component.css']
})
export class ManageCommentsComponent implements OnInit {
  blogs: any;

  constructor(private auth:BlogService) { }

  ngOnInit() {
       this.getBlogs();
  }

  getBlogs() {
    this.auth.listBlog().subscribe(res => {
      this.blogs = res;
    });
  }

}
