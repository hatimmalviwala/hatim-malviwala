import { Component, OnInit } from '@angular/core';
import { FormGroup } from "@angular/forms";
import { FormControl } from "@angular/forms";
import { Validators } from "@angular/forms";
import { ProductService } from "src/app/library/service/product.service";
import { ToastrService } from 'ngx-toastr';
import { FormBuilder } from "@angular/forms";
@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.css']
})
export class AddProductComponent implements OnInit {
  productform: FormGroup;
  submitted = false;

  constructor(private auth: ProductService,private fb :FormBuilder ,private toastr: ToastrService) { }

  ngOnInit() {
    this.productform = this.fb.group({
      productname: new FormControl('', [Validators.required]),
      productcompanyname: new FormControl('', [Validators.required]),
      productdescription: new FormControl('', [Validators.required]),
      productcategory: new FormControl('', Validators.required),
      productcost: new FormControl('', Validators.required),
      file:null,
    });
  }
  onFileChange(event) {
    if (event.target.files.length > 0) {
      let file = event.target.files[0];
      this.productform.get('file').setValue(file);
    }
  }
  private prepareSave(): any {
    let input = new FormData();
    input.append('productname', this.productform.get('productname').value);
    input.append('productcompanyname', this.productform.get('productcompanyname').value);
    input.append('productdescription', this.productform.get('productdescription').value);
    input.append('productcategory', this.productform.get('productcategory').value);
    input.append('productcost', this.productform.get('productcost').value);
    input.append('file', this.productform.get('file').value);
    return input;
  }
  get f() { return this.productform.controls; }
  onsubmit() {
         this.submitted = true;
 
        // stop here if form is invalid
        if (this.productform.invalid) {
            return;
        }    

    const formData = this.prepareSave()
    this.auth.addProduct(formData).subscribe((res: any) => {
      res = res.data;
      console.log('response ::', res);
      // alert("data added successfully");
      this.toastr.success("data added successfully");
    });
  }
}
