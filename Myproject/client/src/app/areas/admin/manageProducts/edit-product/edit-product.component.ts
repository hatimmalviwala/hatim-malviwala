import { Component, OnInit } from '@angular/core';
import { FormGroup } from "@angular/forms";
import { FormControl } from "@angular/forms";
import { Validators } from "@angular/forms";
import { ActivatedRoute, Router } from '@angular/router';
import { ProductService } from 'src/app/library/service/product.service';
import { ToastrService } from 'ngx-toastr';
import { FormBuilder } from "@angular/forms";

@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.css']
})
export class EditProductComponent implements OnInit {
  url: any;
  userdata: any;
  productform: FormGroup;
  submitted = false;


  constructor(private route:ActivatedRoute,
    private fb :FormBuilder,
    private auth:ProductService,
    private toastr:ToastrService,
    private router:Router) { }

  ngOnInit() {
     this.productform = this.fb.group({
      productname: new FormControl('', [Validators.required]),
      productcompanyname: new FormControl('', [Validators.required]),
      productdescription: new FormControl('', [Validators.required]),
      productcategory: new FormControl('', Validators.required),
      productcost: new FormControl('', Validators.required),
      file:null,
    });


   this.route.params.subscribe(params=> {
          this.auth.getProductDetails(params.id).subscribe(res=> {
           this.userdata = res.data;


           console.log(this.userdata)
           this.productform.patchValue({
             productname:this.userdata.productname,
             productcompanyname:this.userdata.productcompanyname,
             productdescription:this.userdata.productdescription,
             productcategory:this.userdata.productcategory,
             productcost:this.userdata.productcost,
           })   
            this.url = this.userdata.imagePath;
          })
        })

   }

  onFileChange(event) {
    if (event.target.files.length > 0) {
      let file = event.target.files[0];
      this.productform.get('file').setValue(file);
    }
  }
  private prepareSave(): any {
    let input = new FormData();
    input.append('productname', this.productform.get('productname').value);
    input.append('productcompanyname', this.productform.get('productcompanyname').value);
    input.append('productdescription', this.productform.get('productdescription').value);
    input.append('productcategory', this.productform.get('productcategory').value);
    input.append('productcost', this.productform.get('productcost').value);
    input.append('file', this.productform.get('file').value);
    return input;
  }

 get f() { return this.productform.controls; }
  
 onsubmit() {
            this.submitted = true;
 
        // stop here if form is invalid
        if (this.productform.invalid) {
            return;
        }    

     const formData = this.prepareSave();
    //  this.submitted = true;
    //     // stop here if form is invalid
    //     if (this.myform.invalid) {
    //         return;
    //     }    
         this.route.params.subscribe(params=>{
          this.auth.editProduct(params.id,formData).subscribe(res=>{
           res = res.data;
           console.log('response ::',res); 
            this.toastr.success("data edited successfully");
          })
        })
        this.router.navigate(['/admin/listProduct']);
   }

}
