import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/library/service/auth.service';
import { ProductService } from "src/app/library/service/product.service";
import { ToastrService } from 'ngx-toastr';
import { ConfirmationService } from "primeng/components/common/confirmationservice";

@Component({
  selector: 'app-list-product',
  templateUrl: './list-product.component.html',
  styleUrls: ['./list-product.component.css']
})
export class ListProductComponent implements OnInit {
products:any;

  constructor(private auth:ProductService,private toastr:ToastrService,private confirmationService: ConfirmationService) { }

  ngOnInit() {
      this.getProducts();
  }
 getProducts() {
    this.auth.listProduct().subscribe(res => {
      this.products = res;
    });
  }
    deleteProduct(id) {
       this.confirmationService.confirm({
            message: 'Do you want to delete this product?',
            header: 'Delete Confirmation',
            icon: 'pi pi-info-circle',
            accept: () => {
              console.log("...id");
              this.auth.deleteProduct(id).subscribe(res => {
              console.log('Deleted',res);
              //   if(res.status == "success"){
              //     console.log("msg",res.msg);
              this.getProducts();
              //   }else{
              //     console.log("fdgdgdfgfd")
              //   }
     });
              this.toastr.success("data deleted successfully");            
            },
            reject: () => {
               //this.toastr.success("not deleted");
            }
        });
    
}
}
