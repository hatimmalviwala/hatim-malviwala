import { Component, OnInit } from '@angular/core';
import { FormGroup } from "@angular/forms";
import { FormControl } from "@angular/forms";
import { Validators } from "@angular/forms";
import { AuthService } from '../../../../library/service/auth.service';
import { ActivatedRoute } from "@angular/router";
import { ToastrService } from 'ngx-toastr';
import { Router } from "@angular/router";

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.css']
})
export class EditUserComponent implements OnInit {
  userdata: any;
  myform: FormGroup;
submitted = false;
  constructor(private route:ActivatedRoute,
    private auth:AuthService,
    private toastr:ToastrService,
    private router:Router) { }

  ngOnInit() {
      this.myform = new FormGroup({
      'fullname': new FormControl('', [Validators.required]),
      'password': new FormControl('', [Validators.required, Validators.minLength(6)]),
      'email': new FormControl('', [Validators.required, Validators.email]),
      'role': new FormControl('', [Validators.required]),
    });
    this.route.params.subscribe(params=> {
          this.auth.getUserDetails(params.id).subscribe(res=> {
           this.userdata = res.data;


           console.log(this.userdata)
           this.myform.patchValue({
             fullname:this.userdata.fullname,
             email:this.userdata.email,
             password:this.userdata.password,
             role:this.userdata.role
           })   
          })
        })

        

  }
 
        
get f() { return this.myform.controls; }

   
   onsubmit() {
     this.submitted = true;
        // stop here if form is invalid
        if (this.myform.invalid) {
            return;
        }    
        this.route.params.subscribe(params=>{
          this.auth.updateUser(params.id,this.myform.value).subscribe(res=>{
           res = res.data;
           console.log('response ::',res); 
          })

        })
        this.toastr.success("user edited successfully");
        this.router.navigate(['/admin/listUser']);
   }
}
