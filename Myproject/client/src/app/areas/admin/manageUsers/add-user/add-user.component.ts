import { Component, OnInit } from '@angular/core';
import { FormGroup } from "@angular/forms";
import { AuthService } from "src/app/library/service/auth.service";
import { Router } from "@angular/router";
import { FormControl } from "@angular/forms";
import { Validators } from "@angular/forms";
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit {
myform: FormGroup;
submitted = false;

  constructor(private auth: AuthService,private router: Router,private toastr:ToastrService) { }

  ngOnInit() {
    this.myform = new FormGroup({
      'fullname': new FormControl('', [Validators.required]),
      'password': new FormControl('', [Validators.required, Validators.minLength(6)]),
      'email': new FormControl('', [Validators.required, Validators.email]),
      'role': new FormControl('', [Validators.required]),
    });
  }
get f() { return this.myform.controls; }
   
   onsubmit() {
     this.submitted = true;
 
        // stop here if form is invalid
        if (this.myform.invalid) {
            return;
        }    
        this.auth.addUser(this.myform.value).subscribe((res: any) => {
        res = res.data;
        console.log('response ::',res);
        this.toastr.success("user added successfully");
      });
  }
}
