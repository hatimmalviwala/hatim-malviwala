import { Component, OnInit } from '@angular/core';
import { AuthService } from "src/app/library/service/auth.service";
import { ToastrService } from 'ngx-toastr';
import { ConfirmationService } from "primeng/components/common/confirmationservice";
@Component({
  selector: 'app-list-users',
  templateUrl: './list-users.component.html',
  styleUrls: ['./list-users.component.css']
})
export class ListUsersComponent implements OnInit {
users:any;
  constructor(private auth:AuthService,private toastr:ToastrService,private confirmationService: ConfirmationService) { }

  ngOnInit() {
     this.getUsers();
  }
    getUsers() {
    this.auth.listuser().subscribe(res => {
      this.users = res;
    });
  }

  deleteUser(id) {
           this.confirmationService.confirm({
            message: 'Do you want to delete this user?',
            header: 'Delete Confirmation',
            icon: 'pi pi-info-circle',
            accept: () => {
    console.log("...id");
     this.auth.deleteUser(id).subscribe(res => {
     console.log('Deleted',res);
    //   if(res.status == "success"){
    //     console.log("msg",res.msg);
         this.getUsers();
    //   }else{
    //     console.log("fdgdgdfgfd")
    //   }
     });
  this.toastr.success("user deleted successfully");
              },
            reject: () => {
               //this.toastr.success("not deleted");
            }
        });

}
}
