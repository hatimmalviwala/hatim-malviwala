import { Component, OnInit } from '@angular/core';
import { FormGroup } from "@angular/forms";
import { FormControl } from "@angular/forms";
import { Validators } from "@angular/forms";
import { FormBuilder } from "@angular/forms";
import { ActivatedRoute, Router } from '@angular/router';
import { BlogService } from '../../../../library/service/blog.service';
import { ToastrModule, ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-edit-blog',
  templateUrl: './edit-blog.component.html',
  styleUrls: ['./edit-blog.component.css']
})
export class EditBlogComponent implements OnInit {
  url: any;
  userdata: any;
  blogform: FormGroup;
submitted = false;

  constructor(private fb:FormBuilder,
    private route:ActivatedRoute,
    private auth:BlogService,
    private toastr:ToastrService,
    private router:Router) { }

  ngOnInit() {
     this.blogform = this.fb.group({
      title: new FormControl('', [Validators.required]),
      description: new FormControl('', [Validators.required]),
      file:null,
    });

    this.route.params.subscribe(params=> {
          this.auth.getBlogDetails(params.id).subscribe(res=> {
           this.userdata = res.data;


           console.log(this.userdata)
           this.blogform.patchValue({
             title:this.userdata.title,
             description:this.userdata.description,
           })   
            this.url = this.userdata.imagePath;
          })
        })

  }
onFileChange(event) {
    if (event.target.files.length > 0) {
      let file = event.target.files[0];
      this.blogform.get('file').setValue(file);
    }
  }
  private prepareSave(): any {
    let input = new FormData();
    input.append('title', this.blogform.get('title').value);
    input.append('description', this.blogform.get('description').value);
    input.append('file', this.blogform.get('file').value);
    return input;
  }
      get f() { return this.blogform.controls; }

     onsubmit() {
                 this.submitted = true;
 
        // stop here if form is invalid
        if (this.blogform.invalid) {
            return;
        }    

     const formData = this.prepareSave();
    //  this.submitted = true;
    //     // stop here if form is invalid
    //     if (this.myform.invalid) {
    //         return;
    //     }    
         this.route.params.subscribe(params=>{
          this.auth.editBlog(params.id,formData).subscribe(res=>{
           res = res.data;
           console.log('response ::',res); 
            this.toastr.success("data edited successfully");
          })
        })
        this.router.navigate(['/admin/listBlog']);

}
}
