import { Component, OnInit } from '@angular/core';
import { BlogService } from '../../../../library/service/blog.service';
import { ToastrService } from 'ngx-toastr';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { ConfirmationService } from "primeng/components/common/confirmationservice";

@Component({
  selector: 'app-list-blog',
  templateUrl: './list-blog.component.html',
  styleUrls: ['./list-blog.component.css']
})
export class ListBlogComponent implements OnInit {
blogs:any;
  constructor(private auth:BlogService,private toastr:ToastrService,private confirmationService: ConfirmationService) { }

  ngOnInit() {
     this.getBlogs();
  }
getBlogs() {
    this.auth.listBlog().subscribe(res => {
    this.blogs = res;
    });
  }
deleteBlog(id) {
         this.confirmationService.confirm({
            message: 'Do you want to delete this blog?',
            header: 'Delete Confirmation',
            icon: 'pi pi-info-circle',
            accept: () => {
    console.log("...id");
     this.auth.deleteBlog(id).subscribe(res => {
     console.log('Deleted',res);
    //   if(res.status == "success"){
    //     console.log("msg",res.msg);
         this.getBlogs();
    //   }else{
    //     console.log("fdgdgdfgfd")
    //   }
     });
     this.toastr.success("data deleted successfully");
            },
            reject: () => {
               //this.toastr.success("not deleted");
            }
        });

    }

 
}
