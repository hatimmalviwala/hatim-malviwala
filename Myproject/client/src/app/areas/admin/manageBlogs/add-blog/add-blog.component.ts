import { Component, OnInit } from '@angular/core';
import { FormGroup } from "@angular/forms";
import { FormBuilder } from "@angular/forms";
import { FormControl } from "@angular/forms";
import { Validators } from "@angular/forms";
import { BlogService } from '../../../../library/service/blog.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-add-blog',
  templateUrl: './add-blog.component.html',
  styleUrls: ['./add-blog.component.css']
})
export class AddBlogComponent implements OnInit {
blogform: FormGroup;
submitted = false;
text = String;
  constructor(private fb:FormBuilder,private auth:BlogService,private toastr:ToastrService) { }

  ngOnInit() {
    this.blogform = this.fb.group({
      title: new FormControl('', [Validators.required]),
      description: new FormControl('', [Validators.required]),
      file:null,
    });
    
  }
  onFileChange(event) {
    if (event.target.files.length > 0) {
      let file = event.target.files[0];
      this.blogform.get('file').setValue(file);
    }
  }
  private prepareSave(): any {
    let input = new FormData();
    input.append('title', this.blogform.get('title').value);
    input.append('description', this.blogform.get('description').value);
    input.append('file', this.blogform.get('file').value);
    return input;
  }
    get f() { return this.blogform.controls; }

  onsubmit() {
            this.submitted = true;
 
        // stop here if form is invalid
        if (this.blogform.invalid) {
            return;
        }    
    const formData = this.prepareSave()
    this.auth.addBlog(formData).subscribe((res: any) => {
      res = res.data;
      console.log('response ::', res);
      // alert("data added successfully")
      this.toastr.success("Blog added successfully");
      
    });
    
  }
}

