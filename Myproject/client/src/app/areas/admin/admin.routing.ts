import { NgModule } from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import { DashboardComponent } from "src/app/areas/admin/dashboard/dashboard.component";
import { AdminComponent } from "src/app/areas/admin/admin.component";
import { AddUserComponent } from "src/app/areas/admin/manageUsers/add-user/add-user.component";
import { ListUsersComponent } from "src/app/areas/admin/manageUsers/list-users/list-users.component";
import { EditUserComponent } from "src/app/areas/admin/manageUsers/edit-user/edit-user.component";
import { ListProductComponent } from "src/app/areas/admin/manageProducts/list-product/list-product.component";
import { AddProductComponent } from "src/app/areas/admin/manageProducts/add-product/add-product.component";
import { EditProductComponent } from "src/app/areas/admin/manageProducts/edit-product/edit-product.component";
import { EditBlogComponent } from "src/app/areas/admin/manageBlogs/edit-blog/edit-blog.component";
import { ListBlogComponent } from "src/app/areas/admin/manageBlogs/list-blog/list-blog.component";
import { AddBlogComponent } from "src/app/areas/admin/manageBlogs/add-blog/add-blog.component";
import { ManageCommentsComponent } from "src/app/areas/admin/manage comments/manage-comments/manage-comments.component";
import { BlogCommentComponent } from "src/app/areas/admin/manage comments/blog-comment/blog-comment.component";


const routes: Routes = [
    {path: '', component: AdminComponent,
    children:[
        {path:"dashboard", component:DashboardComponent},
        {path:"addUser", component:AddUserComponent},
        {path:"listUser", component:ListUsersComponent},
        {path:"edituser/:id", component:EditUserComponent},
        {path:"addProduct", component:AddProductComponent},
        {path:"listProduct", component:ListProductComponent},
        {path:"editProduct/:id", component:EditProductComponent},
        {path:"addBlog", component:AddBlogComponent},
        {path:"listBlog", component:ListBlogComponent},
        {path:"editBlog/:id", component:EditBlogComponent},
        {path:"manageComments", component:ManageCommentsComponent},
        {path:"blogComment/:id", component:BlogCommentComponent}
    ]


},
];


@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
    ],
    exports: [RouterModule],
})
export class AdminRouting {}
