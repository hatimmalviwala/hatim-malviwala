import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AdminRouting } from "src/app/areas/admin/admin.routing";
import { HeaderComponent } from './shared/header/header.component';
import { SidebarComponent } from './shared/sidebar/sidebar.component';
import { AdminComponent } from './admin.component';
import { AddUserComponent } from './manageUsers/add-user/add-user.component';
import { ListUsersComponent } from './manageUsers/list-users/list-users.component';
import { ReactiveFormsModule } from '@angular/forms';
import { EditUserComponent } from './manageUsers/edit-user/edit-user.component';
import { AddProductComponent } from './manageProducts/add-product/add-product.component';
import { ListProductComponent } from './manageProducts/list-product/list-product.component';
import { EditProductComponent } from './manageProducts/edit-product/edit-product.component';
import { AddBlogComponent } from './manageBlogs/add-blog/add-blog.component';
import { ListBlogComponent } from './manageBlogs/list-blog/list-blog.component';
import { EditBlogComponent } from './manageBlogs/edit-blog/edit-blog.component';
import {EditorModule} from 'primeng/editor';
import {ChartModule} from 'primeng/chart';
import {TableModule} from 'primeng/table';
import {PaginatorModule} from 'primeng/paginator';
import { ManageCommentsComponent } from './manage comments/manage-comments/manage-comments.component';
import { BlogCommentComponent } from './manage comments/blog-comment/blog-comment.component';
import {RatingModule} from 'primeng/rating';
import { Ng4LoadingSpinnerModule } from 'ng4-loading-spinner';
import {ConfirmDialogModule} from 'primeng/confirmdialog';


@NgModule({
  imports: [
    CommonModule,
    AdminRouting,
    ReactiveFormsModule,
    EditorModule,
    ChartModule,
    TableModule,
    PaginatorModule,
    RatingModule,    
    Ng4LoadingSpinnerModule,
    ConfirmDialogModule
  ],
  declarations: [DashboardComponent, HeaderComponent, SidebarComponent, AdminComponent, AddUserComponent, ListUsersComponent, EditUserComponent, AddProductComponent, ListProductComponent, EditProductComponent, AddBlogComponent, ListBlogComponent, EditBlogComponent, ManageCommentsComponent, BlogCommentComponent]
})
export class AdminModule { }
