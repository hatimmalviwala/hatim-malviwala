import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  username: string;
  userid :string;

  constructor(private router:Router) { }

  ngOnInit() {
    this.username = localStorage.getItem("fullname");
    this.userid = localStorage.getItem("userid");
  }
logout() {
    localStorage.removeItem('role');
    localStorage.removeItem('token');
    localStorage.removeItem('userid');
    localStorage.removeItem('fullname');
    this.router.navigate(['']);
  }
}
