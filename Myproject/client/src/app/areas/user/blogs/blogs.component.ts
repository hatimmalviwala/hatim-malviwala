import { Component, OnInit } from '@angular/core';
import { BlogService } from "src/app/library/service/blog.service";

@Component({
  selector: 'app-blogs',
  templateUrl: './blogs.component.html',
  styleUrls: ['./blogs.component.css']
})
export class BlogsComponent implements OnInit {
blogs:any;

  constructor(private auth:BlogService) { }

  ngOnInit() {
    this.getBlogs();
  }
getBlogs() {
    this.auth.listBlog().subscribe(res => {
      this.blogs = res;
    });
  }

}
