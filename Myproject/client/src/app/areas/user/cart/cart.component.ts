import { Component, OnInit } from '@angular/core';
import { CartService } from "src/app/library/service/cart.service";
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {
  cartList:any;
  
  constructor(private cart: CartService,private route:ActivatedRoute,private toastr:ToastrService) { }

  ngOnInit() {
      this.getCartDetail()
      
  //     console.log("userid",userid)
  }
getCartDetail(){
       var userid = localStorage.getItem("userid")
      this.cart.getCartDetails(userid).subscribe(res => {
        this.cartList= res;
        console.log("cart details",this.cartList);
    })
}

    deleteCart(id) {
    //console.log("...id");
     this.cart.deleteCart(id).subscribe(res => {
     //console.log('Deleted',res);
    //   if(res.status == "success"){
    //     console.log("msg",res.msg);
         this.getCartDetail();
    //   }else{
    //     console.log("fdgdgdfgfd")
    //   }
     });
  this.toastr.success("product removed");
}
  
}
