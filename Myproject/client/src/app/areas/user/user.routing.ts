import { NgModule } from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import { DashboardComponent } from "src/app/areas/user/dashboard/dashboard.component";
import { UserComponent } from "src/app/areas/user/user.component";
import { BlogsComponent } from "src/app/areas/user/blogs/blogs.component";
import { ProductsComponent } from "src/app/areas/user/products/products.component";
import { BlogDetailComponent } from "src/app/areas/user/blog-detail/blog-detail.component";
import { CartComponent } from "src/app/areas/user/cart/cart.component";


const routes: Routes = [
    {path: '', component: UserComponent,
    children:[
        {path:"dashboard", component:DashboardComponent},
        {path:"blogs", component:BlogsComponent},
        {path:"products", component:ProductsComponent},
        {path:"blogDetail/:id", component:BlogDetailComponent},
        {path:"cart", component:CartComponent }
        
    ]},
];



@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
    ],
    exports: [RouterModule],
})
export class UserRouting {}
