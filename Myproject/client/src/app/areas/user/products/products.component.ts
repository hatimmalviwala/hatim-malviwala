import { Component, OnInit } from '@angular/core';
import { ProductService } from "src/app/library/service/product.service";
import { CartService } from '../../../library/service/cart.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
products:any;
  constructor(private auth: ProductService, private cart: CartService, private toastr: ToastrService) { }

  ngOnInit() {
     this.getProducts();
  }
    getProducts() {
    this.auth.listProduct().subscribe(res => {
      this.products = res;
    });
  }
  onClick(item){
        var data={
        'userid':localStorage.getItem('userid'),
        'productid':item._id,
        'productprice':item.productcost,
      }

      this.cart.addCart(data).subscribe((res: any) => {
      res = res.data;
      console.log('response ::', res);
      this.toastr.success("product added to Cart");
    });
  }
}
