import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard/dashboard.component';
import { UserRouting } from "src/app/areas/user/user.routing";
import { HeaderComponent } from './shared/header/header.component';
 import { UserComponent } from 'src/app/areas/user/user.component';
import { BlogsComponent } from './blogs/blogs.component';
import { ProductsComponent } from './products/products.component';
import { FooterComponent } from './shared/footer/footer.component';
import {CardModule} from 'primeng/card';
import {ButtonModule} from 'primeng/button';
import { BlogDetailComponent } from './blog-detail/blog-detail.component';
import {RatingModule} from 'primeng/rating';
import { ReactiveFormsModule } from "@angular/forms";
import { FormsModule } from "@angular/forms";
import {DataScrollerModule} from 'primeng/datascroller';
import { CartComponent } from './cart/cart.component';

@NgModule({
  imports: [
 
  CommonModule,
    UserRouting,
    CardModule,
    ButtonModule,
    RatingModule,
    FormsModule,
    ReactiveFormsModule,
    DataScrollerModule
  ],
  declarations: [DashboardComponent,UserComponent, HeaderComponent, BlogsComponent, ProductsComponent, FooterComponent, BlogDetailComponent, CartComponent]
})
export class UserModule { }
