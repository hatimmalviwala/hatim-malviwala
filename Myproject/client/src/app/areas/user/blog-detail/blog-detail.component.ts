import { Component, OnInit } from '@angular/core';
import { BlogDetailService } from "src/app/library/service/blog-detail.service";
import { ActivatedRoute } from '@angular/router';
import { BlogService } from "src/app/library/service/blog.service";
import { Router } from "@angular/router";
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-blog-detail',
  templateUrl: './blog-detail.component.html',
  styleUrls: ['./blog-detail.component.css']
})
export class BlogDetailComponent implements OnInit {
  blogList= new Array;
  userList = new Array
  commentList: any
  val : number;
  

  constructor(private blog:BlogService,
    private auth:BlogDetailService,
    private route:ActivatedRoute,
    private router:Router,
    private toastr:ToastrService) { }

  ngOnInit() {
         this.route.params.subscribe(params=> {
          this.blog.getBlogDetails(params.id).subscribe(res=> {
           this.blogList.push(res.data) ;
          })
         })

         this.getblogDetail()
  }
 
  getblogDetail() {
    this.route.params.subscribe(params => {
      this.auth.getBlogDetails(params.id).subscribe(res => {
        this.commentList = res;

        // console.log(this.commentList)
        // for(var i=0;i<this.commentList.length;i++){

        //    this.userList.push(this.commentList[i].userid);
        //    this.commentList[i].fullname = this.userList[i].fullname

        //  }
        // // this.blogList.push(this.commentList[0].blogid)
        // // console.log(this.blogList[0]);
        // console.log(this.commentList);



      })

    })
 }
 onSubmit(){
    var comment = (document.getElementById('comment') as HTMLInputElement).value
   // var rating = (document.getElementById('rating') as HTMLInputElement).value
   // console.log("rating", this.val)
    this.route.params.subscribe(params=>{
      var data={
        'userid':localStorage.getItem('userid'),
        'blogid':params.id,
        'comment':comment,
        'rating' :this.val,
      }
      
      console.log(data)
      this.auth.addComment(data).subscribe(res=>{
        if(res.code == 200){
          this.getblogDetail()
          this.toastr.success("Comment Added");
        }
      })
    })
  }
}
