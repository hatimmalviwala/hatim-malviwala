import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { AppRouting } from "src/app/app.routing";
import { HomeComponent } from "src/app/library/shared/home/home.component";
import { HttpClientModule } from "@angular/common/http";
import { AuthGuard } from './guard/auth.guard';
import { AuthService } from "src/app/library/service/auth.service";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { HTTP_INTERCEPTORS } from "@angular/common/http";
import { InterceptorService } from "src/app/library/service/interceptor/interceptor.service";
import {EditorModule} from 'primeng/editor';
import { Ng4LoadingSpinnerModule } from 'ng4-loading-spinner';
import {ConfirmDialogModule} from 'primeng/confirmdialog';
import {ConfirmationService} from 'primeng/api';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    AppRouting,
    HttpClientModule,
    BrowserAnimationsModule, // required animations module
    ToastrModule.forRoot(),// ToastrModule added
    EditorModule,
    Ng4LoadingSpinnerModule.forRoot(),
    ConfirmDialogModule
  ],
  providers: [AuthService,ConfirmationService,
    { provide: HTTP_INTERCEPTORS,
      useClass:InterceptorService,
      multi: true}],
  bootstrap: [AppComponent]
})
export class AppModule { }
