"use strict"
var mongoose = require('mongoose');

//Define a schema
var Schema = mongoose.Schema;

var formdata = new mongoose.Schema({
    game: { type: String },
    publisher: { type: String },
    released: { type: String },
    rating: { type: Number },
    scores: { type: Number },

});

var games = mongoose.model('games', formdata);

module.export = games;