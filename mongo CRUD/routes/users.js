var express = require('express');
var router = express.Router();
var validator = require('express-validator');
var mongoose = require('mongoose');
var form = mongoose.model('games');
var path = require('path');

// saving data to database
router.post('/', function (req, res, next) {
    console.log(req.body);
    var content = req.body;
    var Form = new form(req.body);
    Form.save(function (err, content) {
        if (err) {
            res.send("error");
        }
        else {
            res.send("Successful");
        }
    });
});

// retreiving data from database for listing
router.get('/GamesDetails', function (req, res, next) {
    form.find(function (err, data) {
        res.send(data);

    })
});

// deleting a particular record from database
router.delete('/GameDelete', function (req, res, next) {
    var id = req.body.deleteid;
    form.findByIdAndRemove({ _id: id }, function (err, data) {
        if (err) {
            throw err;
        }
        else {
            console.log("successfully deleted");
        }
    });
});

// getting id from param and rendering updatedetails html page 
router.get('/update', function (req, res, next) {
    id = req.param('id');
    form.findOne({_id:id},function (err, data){
       if (err) {
           throw err;
       } 
        console.log(data);
    })
    res.render("updatedetails.html");
});

// setting new values to existing records.
router.put('/edit', function (req, res, next) {
    var newg = req.body.game;
    var newp = req.body.publisher;
    form.findByIdAndUpdate({ _id: id }, { $set: { game: newg, publisher: newp } }, function (err, data) {
        if (err) {
            console.log("err", err);
        }
        else {
            console.log("successfully updated");
        }
    })
});
module.exports = router;
