var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var fs = require('fs');
var bodyParser = require('body-parser');
var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var prod_detrouter = require ('./routes/prod_det');
var validator = require('express-validator');


var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');


app.engine('html', function(path, opt, fn) {
   fs.readFile(path, 'utf-8', function(error, str) {
         if (error)
         return str;
         return fn(null, str);
     });
//     console.log(success,"........")
    // var content = JSON.stringify(success);
    // console.log("content..",content)
    // fs.writeFile("example.json",content,'utf8', (err) => {
    // if (err) {
    //     console.error(err);
    //     return;
    // };
    // console.log("File has been created");
// });
 });

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(express.static(path.join(__dirname, 'views')));
app.use(validator());
app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/prod_det', prod_detrouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;

