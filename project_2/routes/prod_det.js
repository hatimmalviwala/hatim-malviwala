var express = require('express');
var router = express.Router();
var fs = require('fs');


router.get('/', function (req, res, next) {
    res.render('prod_details.html');
});
router.get('/getProductList', function (req, res, next) {
    var content;
    fs.readFile('./example.json', 'utf-8', function read(err, data) {
        if (err) {
            throw err;
        }

        content = data;
        // console.log(content);
        res.json(content);

    });
});
module.exports = router;
